package com.github.dant3.paperboy.android.app;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import com.github.dant3.paperboy.android.R;

@ToString
public final class BuildInfo {
    @Getter private final String applicationName;
    @Getter private final String packageName;
    @Getter private final String versionName;
    @Getter private final int versionCode;
    @Getter private final String commitSha;
    @Getter private final String branch;
    @Getter private final long timestamp;
    @Getter private final boolean debugable;
    @Getter private final boolean unitTestingEnvironment;


    public BuildInfo(Context context) {
        String packageName = context.getPackageName();
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, PackageManager.GET_META_DATA);

            Resources resources = context.getResources();

            this.applicationName = resources.getString(R.string.application_name);
            this.packageName = resources.getString(R.string.application_package);
            this.versionName = resources.getString(R.string.application_version);
            this.versionCode = Integer.parseInt(resources.getString(R.string.application_buildNumber));

            String timestampString = resources.getString(R.string.build_timestamp);
            timestampString = timestampString.substring(1);

            this.timestamp = StringUtils.isBlank(timestampString) ? 0 : Long.parseLong(timestampString);
            this.commitSha = resources.getString(R.string.build_SHA);
            this.branch = resources.getString(R.string.build_branch);

            this.debugable = (packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
            this.unitTestingEnvironment = detectIsUnitTestingEnvironment();

        } catch (PackageManager.NameNotFoundException ex) {
            throw new IllegalStateException("Failed to find application package: " + packageName, ex);
        }
    }

    private static boolean detectIsUnitTestingEnvironment() {
        try {
            return Class.forName("org.robolectric.Robolectric") != null;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
