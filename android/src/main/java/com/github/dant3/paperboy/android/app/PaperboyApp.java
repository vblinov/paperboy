package com.github.dant3.paperboy.android.app;

import android.app.Application;
import com.github.dant3.paperboy.android.utils.GuiThreadExecutor;
import com.github.dant3.paperboy.android.utils.SupplierWithContext;
import com.github.dant3.paperboy.core.client.FeedProvider;
import com.github.dant3.paperboy.core.client.SimpleClientContext;
import com.github.dant3.paperboy.core.connection.apache.ApacheHttpConnection;
import com.github.dant3.paperboy.core.format.Provider;
import com.github.dant3.paperboy.core.format.RSS2Provider;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class PaperboyApp extends Application {
    private static final AtomicReference<PaperboyApp> instance = new AtomicReference<PaperboyApp>();
    private final Supplier<BuildInfo> buildInfoSupplier = Suppliers.memoize(new SupplierWithContext<BuildInfo>(this) {
        @Override
        public BuildInfo get() {
            return new BuildInfo(getContext());
        }
    });
    private final Supplier<Executor> guiThreadExecutorSupplier = Suppliers.memoize(new SupplierWithContext<Executor>(this) {
        @Override
        public Executor get() {
            return new GuiThreadExecutor();
        }
    });
    @Getter private final FeedProvider feedProvider;

    public PaperboyApp() {
        super();
        instance.set(this);
        feedProvider = new FeedProvider(
                SimpleClientContext.builder()
                        .connection(new ApacheHttpConnection("Paperboy v.1.0"))
                        .formatProviders(Arrays.<Provider>asList(new RSS2Provider()))
                .parserSupplier(new XmlParserSupplier()).build()
        );

    }

    public static PaperboyApp getInstance() {
        return instance.get();
    }

    public BuildInfo getBuildInfo() {
        return buildInfoSupplier.get();
    }

    public Executor getGuiThreadExecutor() {
        return guiThreadExecutorSupplier.get();
    }

    private static class XmlParserSupplier implements Supplier<XmlPullParser> {
        private static final XmlPullParserFactory factory = createFactory();

        @Override
        public XmlPullParser get() {
            try {
                return factory.newPullParser();
            } catch (XmlPullParserException e) {
                log.debug("Caught exception on trying to create new XmlPullParser", e);
                return null;
            }
        }

        private static XmlPullParserFactory createFactory() {
            try {
                return XmlPullParserFactory.newInstance();
            } catch (XmlPullParserException e) {
                log.debug("Caught exception while trying to create " + XmlPullParserFactory.class.getSimpleName(), e);
                return null;
            }
        }
    }
}