package com.github.dant3.paperboy.android.app.data;

import android.view.View;
import android.view.ViewGroup;
import com.github.dant3.paperboy.android.app.PaperboyApp;
import com.github.dant3.paperboy.android.ui.view.FeedItemView;
import com.github.dant3.paperboy.core.client.FeedProvider;
import com.github.dant3.paperboy.core.struct.Feed;
import com.github.dant3.paperboy.core.struct.Item;
import com.github.dant3.paperboy.utils.CodeStyle;
import com.github.dant3.paperboy.utils.Utils;
import com.google.common.base.Function;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import lombok.Getter;
import org.apache.commons.lang3.Validate;

import java.net.URI;
import java.util.concurrent.atomic.AtomicReference;

public class FeedItemDataAdapter extends RefreshableDataAdapter<Item> {
    @Getter private final URI feedUri;
    private final FeedProvider feedProvider;
    private final AtomicReference<Feed> feed = new AtomicReference<Feed>();
    private final FeedContentConsumer contentConsumer = new FeedContentConsumer();

    public FeedItemDataAdapter(URI feedUri) {
        super();
        this.feedUri = Validate.notNull(feedUri);
        this.feedProvider = PaperboyApp.getInstance().getFeedProvider();

    }

    @Override
    public int getCount() {
        Feed feed = this.feed.get();
        if (feed == null) {
            return 0;
        } else {
            return feed.getChannel().getItems().size();
        }
    }

    @Override
    public Item getItem(int position) {
        Feed feed = this.feed.get();
        if (feed == null) {
            return CodeStyle.stub();
        } else {
            return feed.getChannel().getItems().get(position);
        }
    }

    @Override
    protected ListenableFuture<Void> performRefresh() {
        return Futures.transform(feedProvider.getFeed(feedUri), contentConsumer);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    @edu.umd.cs.findbugs.annotations.SuppressWarnings(value = "BC_UNCONFIRMED_CAST")
    public View getView(int position, View convertView, ViewGroup parent) {
        FeedItemView feedItemView = convertView == null ? FeedItemView.create(parent.getContext()) : (FeedItemView) convertView;
        Item feedItem = getItem(position);
        feedItemView.setData(feedItem);
        return feedItemView;
    }


    private class FeedContentConsumer implements Function<Feed, Void> {
        @Override
        public Void apply(Feed input) {
            FeedItemDataAdapter.this.feed.set(input);
            return Utils.VOID;
        }
    }
}
