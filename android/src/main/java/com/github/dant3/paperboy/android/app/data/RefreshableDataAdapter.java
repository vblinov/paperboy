package com.github.dant3.paperboy.android.app.data;

import android.widget.BaseAdapter;
import com.github.dant3.paperboy.android.app.PaperboyApp;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.ListenableFuture;
import org.apache.commons.lang3.ObjectUtils;

import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

public abstract class RefreshableDataAdapter<T> extends BaseAdapter {
    private final AtomicReference<RefreshState> refreshState = new AtomicReference<RefreshState>(RefreshState.Idle);
    private final Set<RefreshStateListener> listeners = Sets.newHashSet();
    private ListenableFuture<Void> ongoingRefresh;
    private final Executor callbackExecutor;

    public RefreshableDataAdapter() {
        super();
        callbackExecutor = PaperboyApp.getInstance().getGuiThreadExecutor();
    }

    public RefreshState getRefreshState() {
        return refreshState.get();
    }

    public void addRefreshStateListener(RefreshStateListener listener) {
        listeners.add(listener);
    }

    public void removeRefreshStateListener(RefreshStateListener listener) {
        listeners.remove(listener);
    }

    @Override
    public abstract T getItem(int position);

    public ListenableFuture<Void> refresh() {
        if (ongoingRefresh == null) {
            setRefreshState(RefreshState.Refreshing);
            ongoingRefresh = performRefresh();
            ongoingRefresh.addListener(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                    setRefreshState(RefreshState.Idle);
                }
            }, callbackExecutor);
        }
        return ongoingRefresh;
    }

    protected abstract ListenableFuture<Void> performRefresh();

    protected void setRefreshState(RefreshState refreshState) {
        RefreshState oldState = this.refreshState.getAndSet(refreshState);
        if (ObjectUtils.notEqual(oldState, refreshState)) {
            notifyRefreshStateListeners(refreshState);
        }
    }

    private void notifyRefreshStateListeners(RefreshState state) {
        for (RefreshStateListener listener : listeners) {
            listener.onRefreshStateChanged(state);
        }
    }

    public static enum RefreshState {
        Idle,
        Refreshing
    }

    public interface RefreshStateListener {
        void onRefreshStateChanged(RefreshState state);
    }
}
