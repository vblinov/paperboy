package com.github.dant3.paperboy.android.app.infrastructure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicBoolean;

public final class FatalExceptionLogger implements Thread.UncaughtExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(FatalExceptionLogger.class.getSimpleName());
    private final Thread.UncaughtExceptionHandler previousExceptionHandler;
    private static final AtomicBoolean configured = new AtomicBoolean(false);

    private FatalExceptionLogger(Thread.UncaughtExceptionHandler previousExceptionHandler) {
        this.previousExceptionHandler = previousExceptionHandler;
    }

    public static void setup() {
        if (!configured.getAndSet(true)) {
            FatalExceptionLogger fatalExceptionLogger =
                    new FatalExceptionLogger(Thread.getDefaultUncaughtExceptionHandler());
            Thread.setDefaultUncaughtExceptionHandler(fatalExceptionLogger);
            log.info("FatalExceptionLogger is setted up");
        }
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        try {
            log.debug("Uncaught exception!", ex);
        } catch (Throwable error) {
            handleWithParent(thread, error);
        } finally {
            handleWithParent(thread, ex);
        }
    }

    private void handleWithParent(Thread thread, Throwable ex) {
        if (previousExceptionHandler != null) {
            previousExceptionHandler.uncaughtException(thread, ex);
        }
    }
}
