package com.github.dant3.paperboy.android.app.infrastructure;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.android.LogcatAppender;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy;
import com.github.dant3.paperboy.android.app.BuildInfo;
import com.github.dant3.paperboy.utils.CodeStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public final class LoggerConfiguration {
    private static final String logfilePattern =
            "%date{dd MMM yyyy;HH:mm:ss.SSS} | [%thread{}] %-5level | %.36logger{36} - %msg%n";

    private LoggerConfiguration() {
        CodeStyle.stub();
    }

    public static void setup(android.content.Context androidContext) {
        BuildInfo buildInfo = new BuildInfo(androidContext);
        ch.qos.logback.classic.Logger rootLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        LoggerContext loggerContext = rootLogger.getLoggerContext();
        loggerContext.reset();

        if (buildInfo.isDebugable()) {
            // warning: logcat appender is slower due to used pattern
            rootLogger.addAppender(createLogcatAppender(loggerContext));
            rootLogger.setLevel(Level.DEBUG);
        } else {
            rootLogger.addAppender(createFileAppender(loggerContext, androidContext));
            rootLogger.setLevel(Level.DEBUG);
        }
    }

    public static File logDirectory(android.content.Context androidContext) {
        return androidContext.getExternalFilesDir("logs");
    }

    public static String logFilePath(android.content.Context androidContext) {
        return logDirectory(androidContext).getAbsolutePath().concat("/paperboy.0.log");
    }

    private static Appender<ILoggingEvent> createFileAppender(Context loggerContext, android.content.Context androidContext) {
        RollingFileAppender<ILoggingEvent> fileAppender = new RollingFileAppender<ILoggingEvent>();
        fileAppender.setContext(loggerContext);
        fileAppender.setPrudent(false);
        fileAppender.setAppend(true);
        fileAppender.setFile(logFilePath(androidContext));

        FixedWindowRollingPolicy rollingPolicy = new FixedWindowRollingPolicy();
        rollingPolicy.setContext(loggerContext);
        rollingPolicy.setParent(fileAppender);
        rollingPolicy.setFileNamePattern(logDirectory(androidContext).getAbsolutePath() + "/paperboy.%i.log.zip");
        rollingPolicy.setMinIndex(1);
        rollingPolicy.setMaxIndex(5);
        rollingPolicy.start();

        SizeBasedTriggeringPolicy<ILoggingEvent> triggeringPolicy =
                new SizeBasedTriggeringPolicy<ILoggingEvent>();
        triggeringPolicy.setContext(loggerContext);
        triggeringPolicy.start();

        fileAppender.setRollingPolicy(rollingPolicy);
        fileAppender.setTriggeringPolicy(triggeringPolicy);
        fileAppender.setEncoder(createPatternLayoutEncoder(logfilePattern, loggerContext));
        fileAppender.start();
        return fileAppender;
    }

    private static Appender<ILoggingEvent> createLogcatAppender(Context loggerContext) {
        LogcatAppender appender = new LogcatAppender();
        appender.setContext(loggerContext);
        appender.setEncoder(createPatternLayoutEncoder(
                "[%thread{}] %logger{20}.%method{}\\(\\):%line{} - %msg{}%n{}",
                loggerContext
        ));
        appender.setTagEncoder(createPatternLayoutEncoder("PAPERBOY%nopex{}", loggerContext));
        appender.start();
        return appender;
    }

    private static PatternLayoutEncoder createPatternLayoutEncoder(String pattern, Context loggerContext) {
        PatternLayoutEncoder patternLayoutEncoder = new PatternLayoutEncoder();
        patternLayoutEncoder.setContext(loggerContext);
        patternLayoutEncoder.setPattern(pattern);
        patternLayoutEncoder.start();
        return patternLayoutEncoder;
    }
}
