package com.github.dant3.paperboy.android.app.infrastructure;

import android.os.StrictMode;
import com.github.dant3.paperboy.android.app.BuildInfo;
import com.github.dant3.paperboy.utils.CodeStyle;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class ResponsivenessPolicy {
    private static enum Type {
        STRICT,
        LAX,
        NONE
    }

    private ResponsivenessPolicy() {
        CodeStyle.stub();
    }

    public static void setup(BuildInfo buildInfo) {
        ResponsivenessPolicy.Type policyType = selectPolicyType(buildInfo);
        log.debug("Responsiveness policy is {}", policyType);
        setup(selectPolicyType(buildInfo));
    }

    public static void setup(Type policyType) {
        switch (policyType) {
            case STRICT:
                setupStrictPolicy();
                break;
            case LAX:
                setupLaxPolicy();
                break;
            case NONE:
            default:
                // no setup
                break;
        }
    }

    public static void setupStrictPolicy() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDialog()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());
    }

    public static void setupLaxPolicy() {
        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.LAX);
        StrictMode.setVmPolicy(StrictMode.VmPolicy.LAX);
    }

    private static Type selectPolicyType(BuildInfo buildInfo) {
        if (buildInfo.isUnitTestingEnvironment()) {
            return Type.NONE;
        } else if (buildInfo.isDebugable()) {
            return Type.STRICT;
        } else {
            return Type.LAX;
        }
    }
}
