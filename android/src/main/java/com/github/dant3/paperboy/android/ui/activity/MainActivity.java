package com.github.dant3.paperboy.android.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.View;
import android.widget.ListView;
import com.github.dant3.paperboy.android.R;
import com.github.dant3.paperboy.android.app.PaperboyApp;
import com.github.dant3.paperboy.android.app.data.FeedItemDataAdapter;
import com.github.dant3.paperboy.android.app.data.RefreshableDataAdapter;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

import java.net.URI;

@SuppressLint("Registered")
@OptionsMenu(R.menu.activity_main)
@EActivity(R.layout.activity_main)
public class MainActivity extends Activity implements OnRefreshListener {

    @ViewById(R.id.pull_to_refresh)
    protected PullToRefreshLayout pullToRefreshLayout;

    @ViewById(R.id.items_list)
    protected ListView listView;

    private FeedItemDataAdapter adapter;

    @AfterViews
    public void initialize() {
        ActionBarPullToRefresh.from(this)
                .theseChildrenArePullable(listView)
                .listener(this)
                .setup(pullToRefreshLayout);

        this.adapter = new FeedItemDataAdapter(URI.create("http://habrahabr.ru/rss/hub/scala/"));
        listView.setAdapter(adapter);
        pullToRefreshLayout.setRefreshing(adapter.getRefreshState().equals(RefreshableDataAdapter.RefreshState.Refreshing));
    }

    @Override
    @SuppressWarnings("PMD.CompareObjectsWithEquals")
    public void onRefreshStarted(View view) {
        if (view == listView) {
            refreshData();
        }
    }

    private void refreshData() {
        if (adapter.getRefreshState().equals(RefreshableDataAdapter.RefreshState.Idle)) {
            Futures.addCallback(
                    adapter.refresh(),
                    new FutureCallback<Void>() {
                        @Override
                        public void onSuccess(Void result) {
                            pullToRefreshLayout.setRefreshComplete();
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            pullToRefreshLayout.setRefreshComplete();
                            new AlertDialog.Builder(MainActivity.this)
                                    .setTitle("Error")
                                    .setMessage("Failed to refresh data due to exeception: " + t.toString())
                                    .setPositiveButton("OK", null)
                                    .create().show();
                        }
                    }, PaperboyApp.getInstance().getGuiThreadExecutor()
            );
        }
    }
}
