package com.github.dant3.paperboy.android.ui.view;

import android.content.Context;
import android.text.Html;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.github.dant3.paperboy.android.R;
import com.github.dant3.paperboy.core.struct.Item;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.view_feed_item)
public class FeedItemView extends LinearLayout {

    @ViewById(R.id.item_title)
    protected TextView itemTitleView;

    @ViewById(R.id.item_description)
    protected TextView descriptionView;

    public static FeedItemView create(Context context) {
        return FeedItemView_.build(context);
    }

    public FeedItemView(Context context) {
        super(context);
    }

    public void setData(Item feedItem) {
        itemTitleView.setText(feedItem.getTitle());
        descriptionView.setText(Html.fromHtml(feedItem.getDescription()));
    }
}
