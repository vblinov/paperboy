package com.github.dant3.paperboy.android.utils;

import android.os.Handler;
import android.os.Looper;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.Validate;

import java.util.concurrent.Executor;

public class GuiThreadExecutor implements Executor {
    private final Handler handler;
    private final Looper looper;

    public GuiThreadExecutor() {
        this(Looper.getMainLooper());
    }

    public GuiThreadExecutor(Looper looper) {
        this.looper = Validate.notNull(looper);
        this.handler = new Handler(looper);
    }

    @Override
    public void execute(Runnable command) {
        Thread currentThread = Thread.currentThread();
        if (ObjectUtils.equals(currentThread, looper.getThread())) {
            command.run();
        } else {
            handler.post(command);
        }
    }
}
