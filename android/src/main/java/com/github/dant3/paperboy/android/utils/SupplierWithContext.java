package com.github.dant3.paperboy.android.utils;

import android.content.Context;
import com.google.common.base.Supplier;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(suppressConstructorProperties = true)
public abstract class SupplierWithContext<T> implements Supplier<T> {
    @NonNull @Getter(AccessLevel.PROTECTED) private final Context context;

    public abstract T get();
}
