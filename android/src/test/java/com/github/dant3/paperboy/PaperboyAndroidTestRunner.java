package com.github.dant3.paperboy;

import org.junit.runners.model.InitializationError;
import org.robolectric.AndroidManifest;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.SdkConfig;
import org.robolectric.annotation.Config;
import org.robolectric.res.FsFile;

public class PaperboyAndroidTestRunner extends RobolectricTestRunner {
    public PaperboyAndroidTestRunner(Class<?> testClass) throws InitializationError {
        super(testClass);
    }

    @Override
    protected AndroidManifest createAppManifest(FsFile manifestFile, FsFile resDir, FsFile assetsDir) {
        return new PaperboyAndroidManifest(manifestFile, resDir, assetsDir);
    }

    private static class PaperboyAndroidManifest extends AndroidManifest {
        public PaperboyAndroidManifest(FsFile androidManifestFile, FsFile resDirectory, FsFile assetsDirectory) {
            super(androidManifestFile, resDirectory, assetsDirectory);
        }

        @Override
        public String getRClassName() throws Exception {
            return getRClass().getName();
        }

        @Override
        public Class<?> getRClass() {
            return android.R.class;
        }

        @Override
        public String getPackageName() {
            return android.R.class.getPackage().getName();
        }
    }

    @Override
    protected int pickReportedSdkVersion(Config config, AndroidManifest appManifest) {
        return 18; // robolectric does not support 19 api level yet
    }

    @Override
    protected SdkConfig pickSdkVersion(AndroidManifest appManifest, Config config) {
        return new SdkConfig(pickReportedSdkVersion(config, appManifest));
    }
}
