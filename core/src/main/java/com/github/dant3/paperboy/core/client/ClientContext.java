package com.github.dant3.paperboy.core.client;

import com.github.dant3.paperboy.core.connection.HttpConnection;
import com.github.dant3.paperboy.core.format.Provider;
import com.google.common.base.Supplier;
import org.xmlpull.v1.XmlPullParser;

import java.util.List;

public interface ClientContext {
    HttpConnection getConnection();
    List<Provider> getFormatProviders();
    Supplier<XmlPullParser> getParserSupplier();
}
