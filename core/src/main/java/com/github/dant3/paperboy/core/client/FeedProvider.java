package com.github.dant3.paperboy.core.client;

import com.github.dant3.paperboy.core.format.Provider;
import com.github.dant3.paperboy.core.struct.Feed;
import com.google.common.base.Function;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;
import java.net.URI;
import java.util.List;

@RequiredArgsConstructor(suppressConstructorProperties = true)
public class FeedProvider {
    private final FeedParser parser = new FeedParser();
    private final ClientContext context;

    public ListenableFuture<Feed> getFeed(URI uri) {
        return Futures.transform(context.getConnection().getFeed(uri), parser);
    }

    private class FeedParser implements Function<String, Feed> {
        @Override
        @SneakyThrows
        public Feed apply(String input) {
            StringReader reader = new StringReader(input);
            XmlPullParser parser = context.getParserSupplier().get();
            parser.setInput(reader);
            parser.next();
            List<Provider> formatProviders = context.getFormatProviders();
            for (Provider provider : formatProviders) {
                if (provider.canParseStream(parser)) {
                    return provider.parseStream(parser);
                }
            }
            throw new IllegalStateException("Can't parse feed");
        }
    }
}
