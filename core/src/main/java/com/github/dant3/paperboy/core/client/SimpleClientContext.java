package com.github.dant3.paperboy.core.client;

import com.github.dant3.paperboy.core.connection.HttpConnection;
import com.github.dant3.paperboy.core.format.Provider;
import com.google.common.base.Supplier;
import lombok.Value;
import lombok.experimental.Builder;
import org.xmlpull.v1.XmlPullParser;

import java.util.List;

@Value
@Builder(builderClassName = "Builder")
public class SimpleClientContext implements ClientContext {
    private final HttpConnection connection;
    private final List<Provider> formatProviders;
    private final Supplier<XmlPullParser> parserSupplier;
}
