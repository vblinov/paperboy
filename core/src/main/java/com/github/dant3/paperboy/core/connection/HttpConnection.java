package com.github.dant3.paperboy.core.connection;

import com.google.common.util.concurrent.ListenableFuture;

import java.net.URI;

public interface HttpConnection {
    ListenableFuture<String> getFeed(URI uri);
}
