package com.github.dant3.paperboy.core.connection.apache;

import org.apache.http.conn.ssl.SSLSocketFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.Socket;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class ApacheDummySSLSocketFactory extends SSLSocketFactory {
    private static final TrustManager[] TRUST_MANAGERS = new TrustManager[] { new DummyTrustManager() };
    private final SSLContext sslContext = SSLContext.getInstance("TLS");

    public ApacheDummySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
        super(truststore);

        sslContext.init(null, TRUST_MANAGERS, null);
    }

    @Override
    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException {
        return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
    }

    @Override
    public Socket createSocket() throws IOException {
        return sslContext.getSocketFactory().createSocket();
    }

    private static class DummyTrustManager implements X509TrustManager {
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            // STUB!
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            // STUB!
        }

        @SuppressWarnings("PMD.ReturnEmptyArrayRatherThanNull")
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
}
