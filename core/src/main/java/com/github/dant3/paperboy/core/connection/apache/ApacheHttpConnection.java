package com.github.dant3.paperboy.core.connection.apache;

import com.github.dant3.paperboy.core.connection.HttpConnection;
import com.github.dant3.paperboy.utils.SimpleThreadFactory;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.apache.commons.lang3.Validate;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.*;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.security.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ApacheHttpConnection implements HttpConnection {
    private static final Logger log = LoggerFactory.getLogger("HTTP Backend");

    private static final String ACCEPTED_CONTENT_TYPE = "text/xml";
    private static final String DEFAULT_CHARSET = "UTF-8";

    private final HttpClient httpClient;

    private final ListeningExecutorService executorService = MoreExecutors.listeningDecorator(
            Executors.newFixedThreadPool(6, SimpleThreadFactory.createNumerating("HTTP Executor", true))
    );

    public ApacheHttpConnection(String userAgent) {
        this(userAgent, true);
    }

    public ApacheHttpConnection(String userAgent, boolean secure) {
        httpClient = createBaseHttpClient(Validate.notBlank(userAgent), secure);
    }

    private static DefaultHttpClient createBaseHttpClient(String userAgent, boolean secure) {
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setSoTimeout(httpParams, (int) TimeUnit.SECONDS.convert(60, TimeUnit.MILLISECONDS));
        HttpClientParams.setRedirecting(httpParams, true);
        HttpClientParams.setCookiePolicy(httpParams, CookiePolicy.BROWSER_COMPATIBILITY);
        HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(httpParams, DEFAULT_CHARSET);
        HttpProtocolParams.setUserAgent(httpParams, userAgent);
        httpParams.setBooleanParameter(CoreConnectionPNames.STALE_CONNECTION_CHECK, true);

        ClientConnectionManager connectionManager =
                new ThreadSafeClientConnManager(httpParams,createSchemeRegistry(secure));

        return new DefaultHttpClient(connectionManager, httpParams);
    }

    private static SchemeRegistry createSchemeRegistry(boolean secure) {
        SchemeRegistry supportedSchemes = new SchemeRegistry();

        // setup http
        SocketFactory plainSocketFactory = PlainSocketFactory.getSocketFactory();
        supportedSchemes.register(new Scheme("http", plainSocketFactory, 80));

        // setup https
        try {
            if (secure) {
                SSLSocketFactory factory = SSLSocketFactory.getSocketFactory();
                supportedSchemes.register(new Scheme("https", factory, 443));
            } else {
                SSLSocketFactory factory = new ApacheDummySSLSocketFactory(KeyStore.getInstance(KeyStore.getDefaultType()));
                factory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
                supportedSchemes.register(new Scheme("https", factory, 443));
            }
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        return supportedSchemes;
    }

    @Override
    public ListenableFuture<String> getFeed(final URI uri) {
        return executorService.submit(new Callable<String>() {
            @Override
            public String call() throws IOException {
                HttpPost postRequest = new HttpPost(uri);
                postRequest.setHeader("Content-Type", ACCEPTED_CONTENT_TYPE);
                log.debug("GET {}", uri);
                HttpResponse response = httpClient.execute(postRequest);
                return handleResponse(response, uri);
            }
        });
    }

    private static String handleResponse(HttpResponse response, URI uri) throws IOException {
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            throw new IOException("Got response with status " + response.getStatusLine().toString() + ", uri: " + uri.toString());
        }

        HttpEntity responseEntity = response.getEntity();
        if (responseEntity.getContentType().getValue().startsWith(ACCEPTED_CONTENT_TYPE)) {
            String xml = EntityUtils.toString(responseEntity);
            responseEntity.consumeContent();
            log.debug("Response {} --> {}", uri, xml);
            return xml;
        } else {
            responseEntity.consumeContent();
            throw new IOException("Got unexpected content type: " + responseEntity.getContentType().getValue() + "," +
                    " url: " + uri.toString());
        }
    }
}
