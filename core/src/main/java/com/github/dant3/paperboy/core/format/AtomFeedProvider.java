package com.github.dant3.paperboy.core.format;

import com.github.dant3.paperboy.core.struct.Feed;
import com.github.dant3.paperboy.utils.CodeStyle;
import org.xmlpull.v1.XmlPullParser;

import java.io.IOException;

public class AtomFeedProvider implements Provider {
    @Override
    public boolean canParseStream(XmlPullParser parser) {
        return false;
    }

    @Override
    public Feed parseStream(XmlPullParser parser) throws IOException {
        return CodeStyle.stub();
    }
}
