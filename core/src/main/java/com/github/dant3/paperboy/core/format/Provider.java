package com.github.dant3.paperboy.core.format;

import com.github.dant3.paperboy.core.struct.Feed;
import org.xmlpull.v1.XmlPullParser;

import java.io.IOException;

public interface Provider {
    boolean canParseStream(XmlPullParser parser);
    Feed parseStream(XmlPullParser parser) throws IOException;
}
