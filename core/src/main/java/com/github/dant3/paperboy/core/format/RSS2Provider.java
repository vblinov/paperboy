package com.github.dant3.paperboy.core.format;

import com.github.dant3.paperboy.core.struct.*;
import com.github.dant3.paperboy.core.type.RFC822Date;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"PMD.CyclomaticComplexity", "PMD.GodClass"})
public class RSS2Provider implements Provider {
    public static final String TAG_NAME = "rss";
    public static final String VERSION = "2.0";

    @Override
    public boolean canParseStream(XmlPullParser parser) {
        return StringUtils.equals(parser.getName(), TAG_NAME) &&
                StringUtils.equals(parser.getAttributeValue(null, "version"), VERSION);
    }

    @Override
    public Feed parseStream(XmlPullParser parser) throws IOException {
        try {
            Validate.validState(canParseStream(parser));
            Feed feed = new Feed();
            feed.setVersion("2.0");

            int token = XmlPullParser.START_TAG;
            while (token != XmlPullParser.END_DOCUMENT) {
                token = parser.nextToken();
                if (token == XmlPullParser.START_TAG && "channel".equals(parser.getName())) {
                    Channel channel = parseChannel(parser);
                    feed.setChannel(channel);
                } else if (token == XmlPullParser.END_TAG && StringUtils.equals(parser.getName(), TAG_NAME)) {
                    return feed;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private Channel parseChannel(XmlPullParser parser) throws IOException, XmlPullParserException {

        Channel channel = new Channel();
        List<Item> items = new ArrayList<Item>(20); // usually rss feeds contain about 20 entries
        channel.setItems(items);
        List<String> categories = new ArrayList<String>();
        channel.setCategories(categories);

        int token = parser.next();
        String name = parser.getName();

        while ( !(token == XmlPullParser.END_TAG && "channel".equals(name)) ) {
            if (token == XmlPullParser.START_TAG) {
                if ("title".equals(name)) {
                    channel.setTitle(parseStringContainer(parser));
                } else if ("link".equals(name)) {
                    channel.setLink(parseURL(parser));
                } else if ("description".equals(name)) {
                    channel.setDescription(parseStringContainer(parser));
                } else if ("language".equals(name)) {
                    channel.setLanguage(parseStringContainer(parser));
                } else if ("pubDate".equals(name)) {
                    channel.setPubDate(parseDate(parser));
                } else if ("lastBuildDate".equals(name)) {
                    channel.setLastBuildDate(parseDate(parser));
                } else if ("docs".equals(name)) {
                    channel.setDocs(parseURL(parser));
                } else if ("generator".equals(name)) {
                    channel.setGenerator(parseStringContainer(parser));
                } else if ("managingEditor".equals(name)) {
                    channel.setManagingEditor(parseStringContainer(parser));
                } else if ("webMaster".equals(name)) {
                    channel.setWebMaster(parseStringContainer(parser));
                } else if ("image".equals(name)) {
                    channel.setImage(parseImage(parser));
                } else if ("item".equals(name)) {
                    items.add(parseItem(parser));
                } else if ("category".equals(name)) {
                    categories.add(parseStringContainer(parser));
                }
            }

            token = parser.next();
            name = parser.getName();
        }
        return channel;
    }

    private Image parseImage(XmlPullParser parser) throws IOException, XmlPullParserException {
        Image image = new Image();

        int token = parser.nextToken();
        String name = StringUtils.defaultString(parser.getName());

        while ( !(token == XmlPullParser.END_TAG && "image".equals(name)) ) {
            if (token == XmlPullParser.START_TAG) {
                if ("title".equals(name)) {
                    image.setTitle(parseStringContainer(parser));
                } else if ("link".equals(name)) {
                    image.setLink(parseURL(parser));
                } else if ("url".equals(name)) {
                    image.setUrl(parseURL(parser));
                }
            }

            token = parser.nextToken();
            name = StringUtils.defaultString(parser.getName());
        }
        return image;
    }

    private Item parseItem(XmlPullParser parser) throws IOException, XmlPullParserException {
        Item item = new Item();
        List<String> categories = new ArrayList<String>();
        item.setCategories(categories);

        int token = parser.nextToken();
        String name = StringUtils.defaultString(parser.getName());

        while ( !(token == XmlPullParser.END_TAG && "item".equals(name)) ) {
            if (token == XmlPullParser.START_TAG) {
                if ("title".equals(name)) {
                    item.setTitle(parseStringContainer(parser));
                } else if ("link".equals(name)) {
                    item.setLink(parseURL(parser));
                } else if ("description".equals(name)) {
                    item.setDescription(parseStringContainer(parser));
                } else if ("pubDate".equals(name)) {
                    item.setPubDate(parseDate(parser));
                } else if ("guid".equals(name)) {
                    item.setGuid(parseStringContainer(parser));
                } else if ("enclosure".equals(name)) {
                    item.setEnclosure(parseEnclosure(parser));
                } else if ("category".equals(name)) {
                    categories.add(parseStringContainer(parser));
                }
            }

            token = parser.nextToken();
            name = StringUtils.defaultString(parser.getName());
        }
        return item;
    }

    private Enclosure parseEnclosure(XmlPullParser parser) throws MalformedURLException {
        Enclosure enclosure = new Enclosure();
        String urlString = parser.getAttributeValue(null, "url");
        if (StringUtils.isNotBlank(urlString)) {
            enclosure.setUrl(new URL(urlString));
        }
        String length = parser.getAttributeValue(null, "length");
        if (StringUtils.isNotBlank(length)) {
            enclosure.setLength(Long.parseLong(length));
        }
        enclosure.setType(parser.getAttributeValue(null, "type"));
        return enclosure;
    }

    private static URL parseURL(XmlPullParser parser) throws IOException, XmlPullParserException {
        String urlString = parseStringContainer(parser);
        if (StringUtils.isNotBlank(urlString)) {
            return new URL(urlString);
        } else {
            return null;
        }
    }

    private static RFC822Date parseDate(XmlPullParser parser) throws IOException, XmlPullParserException {
        String dateString = parseStringContainer(parser);
        if (StringUtils.isNotBlank(dateString)) {
            return RFC822Date.fromString(dateString);
        } else {
            return null;
        }
    }

    private static String parseStringContainer(XmlPullParser parser) throws IOException, XmlPullParserException {
        int token = parser.next();
        if (token == XmlPullParser.TEXT) {
            return parser.getText();
        } else {
            return null;
        }
    }
}
