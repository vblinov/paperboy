package com.github.dant3.paperboy.core.struct;

import com.github.dant3.paperboy.core.type.RFC822Date;
import com.google.common.collect.Lists;
import lombok.Data;

import java.net.URL;
import java.util.List;

@Data
@SuppressWarnings({"PMD.CommentSize", "PMD.ImmutableField"})
public class Channel {
    private String title;
    private URL link;
    private String description;
    private String language;
    private RFC822Date pubDate;
    private RFC822Date lastBuildDate;
    private URL docs;
    private String generator;
    private String managingEditor;
    private String webMaster;

    private List<Item> items = Lists.newArrayList();

    private List<String> categories = Lists.newArrayList();

    private long ttl;

    private Image image;

    // TODO: add rating
    // see http://www.w3.org/PICS/
    // TODO: add textInput
    // see http://cyber.law.harvard.edu/rss/rss.html#lttextinputgtSubelementOfLtchannelgt
    // TODO: skipHours
    // TODO: skipDays
}
