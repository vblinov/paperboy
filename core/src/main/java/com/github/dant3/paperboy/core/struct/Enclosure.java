package com.github.dant3.paperboy.core.struct;

import lombok.Data;

import java.net.URL;

// <enclosure url="http://www.scripting.com/mp3s/weatherReportSuite.mp3" length="12216320" type="audio/mpeg" />
@Data
public class Enclosure {
    private URL url;
    private long length;
    private String type;
}
