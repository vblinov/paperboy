package com.github.dant3.paperboy.core.struct;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class Feed {
    public static final String EXPECTED_VERSION = "2.0";
    private String version;

    private Channel channel;

    public boolean isValid() {
        return StringUtils.equals(EXPECTED_VERSION, version);
    }
}