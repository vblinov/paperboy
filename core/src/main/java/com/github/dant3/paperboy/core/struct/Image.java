package com.github.dant3.paperboy.core.struct;

import lombok.Data;

import java.net.URL;

@Data
public class Image {
    private URL link;
    private URL url;
    private String title;
}
