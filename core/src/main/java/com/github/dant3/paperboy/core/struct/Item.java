package com.github.dant3.paperboy.core.struct;

import com.github.dant3.paperboy.core.type.RFC822Date;
import com.google.common.collect.Lists;
import lombok.Data;

import java.net.URL;
import java.util.List;

@Data
@SuppressWarnings("PMD.ImmutableField")
public class Item {
    private String title;
    private URL link;
    private String description;
    private RFC822Date pubDate;
    private String guid;
    private String author;

    private List<String> categories = Lists.newArrayList();

    private URL comments;

    private Enclosure enclosure;

    private URL source;
}