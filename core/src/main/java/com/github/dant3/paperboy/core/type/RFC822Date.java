package com.github.dant3.paperboy.core.type;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class RFC822Date implements Comparable<RFC822Date> {
    public static final String FORMAT = "E, dd MMM yyyy HH:mm:ss Z";
    private static final ThreadLocalDateFormat dateFormat = new ThreadLocalDateFormat(FORMAT);

    private final long milliseconds;

    public static RFC822Date now() {
        return new RFC822Date(System.currentTimeMillis());
    }

    public static RFC822Date fromDate(Date date) {
        return new RFC822Date(Validate.notNull(date).getTime());
    }

    public static RFC822Date fromString(String value) {
        try {
            return new RFC822Date(dateFormat.get().parse(value).getTime());
        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    public RFC822Date(long milliseconds) {
        this.milliseconds = milliseconds;
    }

    @Override
    public String toString() {
        return dateFormat.get().format(toDate());
    }

    public boolean equalsToDate(Date other) {
        return other != null && other.getTime() == milliseconds;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof RFC822Date) {
            return milliseconds == ((RFC822Date) other).milliseconds;
        } else {
            return super.equals(other);
        }
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public int compareTo(RFC822Date other) {
        return Long.compare(milliseconds, other.milliseconds);
    }

    public Date toDate() {
        return new Date(milliseconds);
    }


    // As JavaDoc states - SimpleDateFormat is not thread-safe,
    // so we make it ThreadLocal
    private static class ThreadLocalDateFormat extends ThreadLocal<DateFormat> {
        private final String format;

        public ThreadLocalDateFormat(String format) {
            super();
            this.format = Validate.notEmpty(format);
        }

        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat(format, Locale.US);
        }
    }
}
