package com.github.dant3.paperboy.utils;

public final class CodeStyle {
    public static final String STUB = "STUB!";

    private CodeStyle() {
        noop();
    }

    public static void noop() {
        // noop
    }

    public static <T> T stub() {
        return stub(STUB);
    }

    public static <T> T stub(String message) {
        throw stubException(message);
    }

    public static RuntimeException stubException() {
        return stubException(STUB);
    }

    public static RuntimeException stubException(String message) {
        return new UnsupportedOperationException(message);
    }
}