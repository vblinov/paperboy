package com.github.dant3.paperboy.utils;

import java.util.concurrent.TimeUnit;

public class ElapsedTimer {

    private static final long NO_VALUE = -1;

    private long startTimeMillis = NO_VALUE;
    private long elapsedTimeMillis = NO_VALUE;

    public void start() {
        elapsedTimeMillis = NO_VALUE;
        startTimeMillis = System.currentTimeMillis();
    }

    public void stop() {
        elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
        startTimeMillis = NO_VALUE;
    }

    public boolean isRunning() {
        return startTimeMillis != NO_VALUE;
    }

    public long getElapsedMillis() {
        if (elapsedTimeMillis == NO_VALUE) {
            return timeSinceStart();
        } else {
            return elapsedTimeMillis;
        }
    }

    public long getElapsedSeconds() {
        return TimeUnit.SECONDS.convert(getElapsedMillis(), TimeUnit.MILLISECONDS);
    }

    private long timeSinceStart() {
        return startTimeMillis == NO_VALUE ? 0 : System.currentTimeMillis() - startTimeMillis;
    }
}
