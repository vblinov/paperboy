package com.github.dant3.paperboy.utils;

import org.apache.commons.lang3.Validate;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

public final class SimpleThreadFactory implements ThreadFactory {
    private final String threadNameBase;
    private final boolean daemon;
    private final boolean withNumeration;
    private final int priority;
    private final AtomicLong threadNumber = new AtomicLong(1);

    public static SimpleThreadFactory create(String threadsName, boolean isDaemon) {
        return create(threadsName, isDaemon, Thread.NORM_PRIORITY);
    }

    public static SimpleThreadFactory create(String threadsName, boolean isDaemon, int priority) {
        return new SimpleThreadFactory(threadsName, isDaemon, priority, false);
    }

    public static SimpleThreadFactory createNumerating(String threadsName, boolean isDaemon) {
        return createNumerating(threadsName, isDaemon, Thread.NORM_PRIORITY);
    }

    public static SimpleThreadFactory createNumerating(String threadsName, boolean isDaemon, int priority) {
        return new SimpleThreadFactory(threadsName, isDaemon, priority, true);
    }

    private SimpleThreadFactory(String threadsName, boolean isDaemon, int priority, boolean withNumeration) {
        this.threadNameBase = Validate.notNull(threadsName);
        this.daemon = isDaemon;
        this.priority = priority;
        this.withNumeration = withNumeration;
    }

    @Override
    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable, createThreadName());
        thread.setDaemon(daemon);
        thread.setPriority(priority);
        return thread;
    }

    private String createThreadName() {
        if (withNumeration) {
            return threadNameBase + " #" + threadNumber.getAndIncrement();
        } else {
            return threadNameBase;
        }
    }
}
