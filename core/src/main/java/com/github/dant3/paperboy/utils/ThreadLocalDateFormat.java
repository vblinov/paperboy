package com.github.dant3.paperboy.utils;

import lombok.Getter;
import org.apache.commons.lang3.Validate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

// As JavaDoc states - SimpleDateFormat is not thread-safe,
// so we make it ThreadLocal
public class ThreadLocalDateFormat extends ThreadLocal<DateFormat> {
    @Getter private final String format;

    public ThreadLocalDateFormat(String format) {
        super();
        this.format = Validate.notEmpty(format);
    }

    @Override
    protected DateFormat initialValue() {
        return new SimpleDateFormat(format, Locale.US);
    }
}
