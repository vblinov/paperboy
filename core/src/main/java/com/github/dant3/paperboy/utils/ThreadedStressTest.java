package com.github.dant3.paperboy.utils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class ThreadedStressTest {
    private ThreadedStressTest() {
        CodeStyle.stub();
    }

    public static int optimalNumberOfThreads() {
        return Runtime.getRuntime().availableProcessors() * 3;
    }

    public static void run(Runnable whatToRunInThreads) throws InterruptedException {
        run(whatToRunInThreads, optimalNumberOfThreads());
    }

    public static void run(Runnable whatToRunInThreads, int numberOfThreads) throws InterruptedException {
        ExecutorService fixedSizeExecutorService = Executors.newFixedThreadPool(numberOfThreads, SimpleThreadFactory.createNumerating("StressTest Thread", false));
        for (int i = 0; i < numberOfThreads; ++i) {
            fixedSizeExecutorService.execute(whatToRunInThreads);
        }
        blockUntilAllTasksAreCompleted(fixedSizeExecutorService);
    }

    private static void blockUntilAllTasksAreCompleted(ExecutorService executorService) throws InterruptedException {
        AwaitRunnable waiter = new AwaitRunnable();
        executorService.execute(waiter);
        waiter.await();
    }

    private static final class AwaitRunnable implements Runnable {
        private final CountDownLatch latch = new CountDownLatch(1);

        @Override
        public void run() {
            latch.countDown();
        }

        public void await() throws InterruptedException {
            latch.await();
        }
    }
}
