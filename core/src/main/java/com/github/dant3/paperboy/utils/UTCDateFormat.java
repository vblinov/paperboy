package com.github.dant3.paperboy.utils;

import java.text.DateFormat;
import java.util.TimeZone;

public class UTCDateFormat extends ThreadLocalDateFormat {
    public UTCDateFormat(String format) {
        super(format);
    }

    @Override
    protected DateFormat initialValue() {
        DateFormat dateFormat = super.initialValue();
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat;
    }
}
