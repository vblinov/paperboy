package com.github.dant3.paperboy.utils;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Constructor;
import java.net.URI;
import java.util.Random;

@SuppressWarnings("PMD.AvoidLiteralsInIfCondition")
public final class Utils {
    public static final Void VOID = createVoidInstance();

    private Utils() {
        CodeStyle.stub();
    }

    // Human-readable backport of Long.compare(long, long) for Java 6
    public static int compareLong(long left, long right) {
        if (left < right) {
            return -1;
        } else if (left == right) {
            return 0;
        } else {
            return 1;
        }
    }

    public static URI uriWithDefaultPath(URI uri) {
        if (StringUtils.isBlank(uri.getPath())) {
            return uri.resolve("/");
        } else {
            return uri;
        }
    }

    public static Function<Object, Void> alwaysVoid() {
        return VoidTransformer.INSTANCE;
    }

    public static <T extends Comparable<? super T>> boolean isLessThan(T left, T right) {
        return ObjectUtils.compare(left, right) < 0;
    }

    public static  <T extends Comparable<? super T>> boolean isGreaterThan(T left, T right) {
        return ObjectUtils.compare(left, right) > 0;
    }

    @VisibleForTesting
    public static <E extends Enum<E>> E randomEnum(Class<E> enumClass) {
        return new RandomEnum<E>(enumClass).random();
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    private static Void createVoidInstance() {
        try {
            Constructor<Void> constructor = Void.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            return constructor.newInstance();
        } catch (Exception ex) {
            return null;
        }
    }


    @VisibleForTesting
    public static class RandomEnum<E extends Enum<E>> {

        private static final Random RND = new Random();
        private final E[] values;

        public RandomEnum(Class<E> token) {
            values = token.getEnumConstants();
        }

        public E random() {
            return values[RND.nextInt(values.length)];
        }
    }

    public static class VoidTransformer implements Function<Object, Void> {
        public static final VoidTransformer INSTANCE = new VoidTransformer();

        private VoidTransformer() {
            CodeStyle.noop();
        }

        @Override
        public Void apply(Object input) {
            return VOID;
        }
    }
}
