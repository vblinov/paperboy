package com.github.dant3.paperboy.utils.concurrent;

import org.apache.commons.lang3.Validate;

public class CurrentThreadAssertion implements ThreadingAssertion {
    private final Thread thread;

    public static ThreadingAssertion create(Thread thread) {
        if (thread == null) {
            return new DummyThreadAssertion();
        } else {
            return new CurrentThreadAssertion(thread);
        }
    }

    public CurrentThreadAssertion(Thread thread) {
        this.thread = Validate.notNull(thread);
    }

    @Override
    public void check() {
        if (Thread.currentThread() != thread) {
            throw new AssertionError("Expected current thread to be " + thread +
                    " but it was " + Thread.currentThread());
        }
    }
}
