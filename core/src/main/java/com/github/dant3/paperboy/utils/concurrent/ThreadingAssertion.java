package com.github.dant3.paperboy.utils.concurrent;

public interface ThreadingAssertion {
    void check();
}
