package com.github.dant3.paperboy.utils.observers;

import com.google.common.base.Predicate;
import org.apache.commons.lang3.Validate;

public class ConditionalUpdateAwaitingObserver<T extends Observable<T>> extends SingleUpdateAwaitingObserver<T> {
    private final Predicate<T> conditionCheck;

    public ConditionalUpdateAwaitingObserver(Predicate<T> isSatisfied) {
        super();
        this.conditionCheck = Validate.notNull(isSatisfied);
    }

    public ConditionalUpdateAwaitingObserver(Observable<T> observable, Predicate<T> isSatisfied) {
        super(observable);
        this.conditionCheck = Validate.notNull(isSatisfied);
    }

    @Override
    public void handleUpdate(T updatedObject) {
        boolean conditionSatisfied = conditionCheck.apply(updatedObject);
        if (conditionSatisfied) {
            super.handleUpdate(updatedObject);
        }
    }
}
