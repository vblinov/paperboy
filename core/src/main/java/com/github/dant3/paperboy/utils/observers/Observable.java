package com.github.dant3.paperboy.utils.observers;

public interface Observable<T extends Observable<T>> {
    void addObserver(Observer<? super T> observer);
    void removeObserver(Observer<? super T> observer);
    void removeObservers();
    int observersCount();

    void notifyObservers();
}
