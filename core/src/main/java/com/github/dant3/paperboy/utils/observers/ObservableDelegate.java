package com.github.dant3.paperboy.utils.observers;

@SuppressWarnings("PMD")
public class ObservableDelegate<T extends Observable<T>, U> extends ObservableSupport<T> implements Observer<U> {
    protected ObservableDelegate() {
        super();
    }

    public ObservableDelegate(T observable) {
        super(observable);
    }

    @Override
    public void handleUpdate(U updatedObject) {
        notifyObservers();
    }

    @Override
    public void addObserver(Observer<? super T> observer) {
        super.addObserver(observer);
    }

    @Override
    public void removeObserver(Observer<? super T> observer) {
        super.removeObserver(observer);
    }

    @Override
    public void removeObservers() {
        super.removeObservers();
    }

    @Override
    public int observersCount() {
        return super.observersCount();
    }

    @Override
    public void notifyObservers() {
        super.notifyObservers();
    }
}
