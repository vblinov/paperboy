package com.github.dant3.paperboy.utils.observers;

public interface Observer<T> {
    void handleUpdate(T updatedObject);
}
