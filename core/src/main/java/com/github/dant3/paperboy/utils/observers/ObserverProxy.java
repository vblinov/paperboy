package com.github.dant3.paperboy.utils.observers;

import lombok.Getter;
import org.apache.commons.lang3.Validate;

public class ObserverProxy<T extends Observable<T>, U extends Observer<T>> implements Observer<T> {
    @Getter
    private final U delegate;

    public ObserverProxy(U delegate) {
        this.delegate = Validate.notNull(delegate);
    }

    @Override
    public void handleUpdate(T updatedObject) {
        delegate.handleUpdate(updatedObject);
    }
}
