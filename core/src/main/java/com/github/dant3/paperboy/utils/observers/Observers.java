package com.github.dant3.paperboy.utils.observers;

import com.google.common.base.Predicate;
import com.github.dant3.paperboy.utils.CodeStyle;


public final class Observers {
    private Observers() {
        CodeStyle.stub();
    }

    public static <T extends Observable<T>> SingleUpdateAwaitingObserver<T> awaitForNextUpdate(Observable<T> observable) {
        return new SingleUpdateAwaitingObserver<T>(observable);
    }

    public static <T extends Observable<T>> SingleUpdateAwaitingObserver<T> awaitForNextUpdateWithCondition(Observable<T> observable, Predicate<T> condition) {
        return new ConditionalUpdateAwaitingObserver<T>(observable, condition);
    }
}
