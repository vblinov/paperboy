package com.github.dant3.paperboy.utils.observers;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import lombok.Delegate;

public class SingleUpdateAwaitingObserver<T extends Observable<T>> implements Observer<T> {
    private final SettableFuture<T> updateHappened = SettableFuture.create();

    @Delegate
    public ListenableFuture<T> getHandle() {
        return updateHappened;
    }

    public SingleUpdateAwaitingObserver() {
        // to add it as listener manually
    }

    public SingleUpdateAwaitingObserver(Observable<T> observable) {
        observable.addObserver(this);
    }

    @Override
    @SuppressWarnings("unckeched")
    public void handleUpdate(T updatedObject) {
        updateHappened.set(updatedObject);
        updatedObject.removeObserver(this);
    }
}
