package com.github.dant3.paperboy.utils.ssl;

import lombok.extern.slf4j.Slf4j;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

@Slf4j
@SuppressWarnings("PMD")
public class CatchingSSLSocketFactory extends SSLSocketFactory {
    private final SSLSocketFactory factory;

    public CatchingSSLSocketFactory() {
        SSLSocketFactory factory;
        try {
            factory = SSLContext.getDefault().getSocketFactory();
        } catch (Exception ex) {
            throw new IllegalStateException("Cant create socket factory", ex);
        }

        this.factory = factory;
    }

    @Override
    public Socket createSocket(Socket socket, String s, int i, boolean flag)
            throws IOException {
        try {
            return factory.createSocket(socket, s, i, flag);
        } catch (Exception ex) {
            log.error("createSocket exception", ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public Socket createSocket(InetAddress inaddr, int i, InetAddress inaddr2, int j)
            throws IOException {
        try {
            return factory.createSocket(inaddr, i, inaddr2, j);
        } catch (Exception ex) {
            log.error("createSocket exception", ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public Socket createSocket(InetAddress inaddr, int i) throws IOException {
        try {
            return factory.createSocket(inaddr, i);
        } catch (Exception ex) {
            log.error("createSocket exception", ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public Socket createSocket(String s, int i, InetAddress inaddr, int j) throws IOException {
        try {
            return factory.createSocket(s, i, inaddr, j);
        } catch (Exception ex) {
            log.error("createSocket exception", ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public Socket createSocket(String s, int i) throws IOException {
        try {
            return factory.createSocket(s, i);
        } catch (Exception ex) {
            log.error("createSocket exception", ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public String[] getDefaultCipherSuites() {
        try {
            return factory.getSupportedCipherSuites();
        } catch (Exception ex) {
            log.error("getDefaultCipherSuites exception", ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public String[] getSupportedCipherSuites() {
        try {
            return factory.getSupportedCipherSuites();
        } catch (Exception ex) {
            log.error("getSupportedCipherSuites exception", ex);
            throw new IllegalStateException(ex);
        }
    }
}
