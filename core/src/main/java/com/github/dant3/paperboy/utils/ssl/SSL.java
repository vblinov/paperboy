package com.github.dant3.paperboy.utils.ssl;

import com.github.dant3.paperboy.utils.CodeStyle;
import lombok.extern.slf4j.Slf4j;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

@Slf4j
public final class SSL {
    private static final TrustManager[] TRUST_MANAGERS = new TrustManager[] { new DummyTrustManager() };
    private static final SSLContext uncheckedSSSLContext = tryToCreateSSLContext();

    public static SSLContext getUncheckedSSSLContext() {
        return uncheckedSSSLContext;
    }

    private SSL() {
        CodeStyle.stub();
    }

    private static SSLContext tryToCreateSSLContext() {
        try {
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, TRUST_MANAGERS, new SecureRandom());
            return context;
        } catch (NoSuchAlgorithmException e) {
            SSL.log.debug("Could not create unchecked SSL Context due to exception", e);
            return null;
        } catch (KeyManagementException e) {
            SSL.log.debug("Could not init unchecked SSL Context due to exception", e);
            return null;
        }
    }


    private static class DummyTrustManager implements X509TrustManager {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            // STUB that does nothing
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            // STUB that does nothing
        }

        @Override
        @SuppressWarnings("PMD.ReturnEmptyArrayRatherThanNull")
        // we don't want to mess with tons of pointless empty arrays in heap
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
}
