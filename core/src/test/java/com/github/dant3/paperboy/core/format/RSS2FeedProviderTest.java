package com.github.dant3.paperboy.core.format;

import com.github.dant3.paperboy.core.struct.Channel;
import com.github.dant3.paperboy.core.struct.Feed;
import com.github.dant3.paperboy.core.struct.Item;
import com.github.dant3.paperboy.core.type.RFC822Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xmlpull.mxp1.MXParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class RSS2FeedProviderTest {
    private InputStream rss2feedSource;
    private RSS2Provider provider;
    private Feed sampleFeed;

    @Before
    public void setUp() {
        rss2feedSource = getClass().getResourceAsStream("rss2feed.xml");
        provider = new RSS2Provider();
        sampleFeed = createSampleFeed();
    }

    @After
    public void tearDown() {
        if (rss2feedSource != null) {
            try {
                rss2feedSource.close();
            } catch (IOException e) {
                // just ignore
            } finally {
                rss2feedSource = null;
            }
        }
    }

    @Test
    public void sampleFeedEqualsToItself() {
        assertEquals(sampleFeed, sampleFeed);
    }

    @Test
    public void deserializedWell() throws IOException, XmlPullParserException {
        XmlPullParser parser = createParser(rss2feedSource);
        // move parser to beginning of feed
        int token = parser.next();
        assertEquals(XmlPullParser.START_TAG, token);
        assertEquals("rss", parser.getName());

        Feed feed = provider.parseStream(parser);
        assertNotNull(feed);
        assertTrue(feed.isValid());
        assertEquals(sampleFeed, feed);
    }

    @Test
    public void canHandleHabraFeed() throws IOException, XmlPullParserException {
        XmlPullParser parser = createParser(getClass().getResourceAsStream("habrafeed.xml"));
        // move parser to beginning of feed
        int token = parser.next();
        assertEquals(XmlPullParser.START_TAG, token);
        assertEquals("rss", parser.getName());

        Feed feed = provider.parseStream(parser);
        assertNotNull(feed);
    }

    private static Feed createSampleFeed() {
        Feed feed = new Feed();
        feed.setVersion(Feed.EXPECTED_VERSION);
        feed.setChannel(createSampleChannel());
        return feed;
    }

    private static Channel createSampleChannel() {
        try {
            Channel channel = new Channel();
            channel.setTitle("Liftoff News");
            channel.setLink(new URL("http://liftoff.msfc.nasa.gov/"));
            channel.setDescription("Liftoff to Space Exploration.");
            channel.setLanguage("en-us");
            channel.setPubDate(RFC822Date.fromString("Tue, 10 Jun 2003 04:00:00 GMT"));
            channel.setLastBuildDate(RFC822Date.fromString("Tue, 10 Jun 2003 09:41:01 GMT"));
            channel.setDocs(new URL("http://blogs.law.harvard.edu/tech/rss"));
            channel.setGenerator("Weblog Editor 2.0");
            channel.setManagingEditor("editor@example.com");
            channel.setWebMaster("webmaster@example.com");

            channel.setItems(Arrays.asList(createSampleItem1(), createSampleItem2()));

            return channel;
        } catch (MalformedURLException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    private static Item createSampleItem1() throws MalformedURLException {
        Item item = new Item();
        item.setTitle("Star City");
        item.setLink(new URL("http://liftoff.msfc.nasa.gov/news/2003/news-starcity.asp"));
        item.setDescription("How do Americans get ready to work with Russians aboard the\n" +
                "                International Space Station? They take a crash course in culture, language\n" +
                "                and protocol at Russia's Star City.");
        item.setPubDate(RFC822Date.fromString("Tue, 03 Jun 2003 09:39:21 GMT"));
        item.setGuid("http://liftoff.msfc.nasa.gov/2003/06/03.html#item573");
        return item;
    }

    private static Item createSampleItem2() throws MalformedURLException {
        Item item = new Item();
        item.setTitle("Space Exploration");
        item.setLink(new URL("http://liftoff.msfc.nasa.gov/"));
        item.setDescription("Sky watchers in Europe, Asia, and parts of Alaska and Canada\n" +
                "                will experience a partial eclipse of the Sun on Saturday, May 31st.");
        item.setPubDate(RFC822Date.fromString("Fri, 30 May 2003 11:06:42 GMT"));
        item.setGuid("http://liftoff.msfc.nasa.gov/2003/05/30.html#item572");
        return item;
    }

    private XmlPullParser createParser(InputStream inputStream) throws XmlPullParserException {
        MXParser parser = new MXParser();
        parser.setInput(inputStream, "UTF-8");
        return parser;
    }
}
