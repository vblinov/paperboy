package com.github.dant3.paperboy.utils;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.*;

public class ElapsedTimerTest {
    private ElapsedTimer elapsedTimer;

    @Before
    public void setUp() {
        elapsedTimer = new ElapsedTimer();
    }

    @Test
    public void reportsZeroElapsedTimeBeforeStarted() {
        assertFalse(elapsedTimer.isRunning());
        assertThat(elapsedTimer.getElapsedMillis(), is(equalTo(0L)));
    }

    @Test
    public void canReportElapsedTimeWhileRunning() throws InterruptedException {
        assertFalse(elapsedTimer.isRunning());
        elapsedTimer.start();

        assertTrue(elapsedTimer.isRunning());
        TimeUnit.MILLISECONDS.sleep(5);

        long firstElapsedTimeSnapshot = elapsedTimer.getElapsedMillis();
        assertThat(firstElapsedTimeSnapshot, is(greaterThan(0L)));

        TimeUnit.MILLISECONDS.sleep(5);

        long secondElapsedTimeSnapshot = elapsedTimer.getElapsedMillis();
        assertThat(secondElapsedTimeSnapshot, is(greaterThan(0L)));
        assertThat(secondElapsedTimeSnapshot, is(greaterThan(firstElapsedTimeSnapshot)));
    }

    @Test
    public void reportsSameElapsedTimeAfterStopped() throws InterruptedException {
        assertFalse(elapsedTimer.isRunning());
        elapsedTimer.start();

        assertTrue(elapsedTimer.isRunning());
        TimeUnit.MILLISECONDS.sleep(5);
        elapsedTimer.stop();

        long firstElapsedTimeSnapshot = elapsedTimer.getElapsedMillis();
        assertThat(firstElapsedTimeSnapshot, is(greaterThan(0L)));
        TimeUnit.MILLISECONDS.sleep(5);

        long secondElapsedTimeSnapshot = elapsedTimer.getElapsedMillis();
        assertThat(secondElapsedTimeSnapshot, is(greaterThan(0L)));
        assertThat(secondElapsedTimeSnapshot, is(equalTo(firstElapsedTimeSnapshot)));
    }
}
