package com.github.dant3.paperboy.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class SimpleThreadFactoryTest {

    private static final String RANDOM_THREAD_NAME = RandomStringUtils.randomAlphanumeric(42);
    private static final boolean IS_DAEMON = false;
    private static final int DEFAULT_PRIORITY = Thread.NORM_PRIORITY;
    private static final int DESIRED_PRIORITY = Thread.MIN_PRIORITY;

    @Test
    public void createsNonNumeratedThreadsWithNomralPriority() {
        SimpleThreadFactory threadFactory = SimpleThreadFactory.create(RANDOM_THREAD_NAME, IS_DAEMON);

        for (int i = 0; i < 10; ++i) {
            Thread thread = threadFactory.newThread(Mockito.mock(Runnable.class));

            assertThat(thread.getName(), is(equalTo(RANDOM_THREAD_NAME)));
            assertThat(thread.isDaemon(), is(equalTo(IS_DAEMON)));
            assertThat(thread.getPriority(), is(equalTo(DEFAULT_PRIORITY)));
        }
    }

    @Test
    public void createsNonNumeratedThreadsWithDesiredPriority() {
        SimpleThreadFactory threadFactory = SimpleThreadFactory.create(RANDOM_THREAD_NAME, IS_DAEMON, DESIRED_PRIORITY);

        for (int i = 0; i < 10; ++i) {
            Thread thread = threadFactory.newThread(Mockito.mock(Runnable.class));

            assertThat(thread.getName(), is(equalTo(RANDOM_THREAD_NAME)));
            assertThat(thread.isDaemon(), is(equalTo(IS_DAEMON)));
            assertThat(thread.getPriority(), is(equalTo(DESIRED_PRIORITY)));
        }
    }

    @Test
    public void createsNumeratedThreadsWithNomralPriority() {
        SimpleThreadFactory threadFactory = SimpleThreadFactory.createNumerating(RANDOM_THREAD_NAME, IS_DAEMON);

        for (int threadIndex = 1; threadIndex <= 10; ++threadIndex) {
            Thread thread = threadFactory.newThread(Mockito.mock(Runnable.class));

            assertThat(thread.getName(), is(startsWith(RANDOM_THREAD_NAME)));
            assertThat(thread.getName(), is(endsWith(String.valueOf(threadIndex))));
            assertThat(thread.isDaemon(), is(equalTo(IS_DAEMON)));
            assertThat(thread.getPriority(), is(equalTo(DEFAULT_PRIORITY)));
        }
    }

    @Test
    public void createsNumeratedThreadsWithDesiredPriority() {
        SimpleThreadFactory threadFactory = SimpleThreadFactory.createNumerating(RANDOM_THREAD_NAME, IS_DAEMON, DESIRED_PRIORITY);

        for (int threadIndex = 1; threadIndex <= 10; ++threadIndex) {
            Thread thread = threadFactory.newThread(Mockito.mock(Runnable.class));

            assertThat(thread.getName(), is(startsWith(RANDOM_THREAD_NAME)));
            assertThat(thread.getName(), is(endsWith(String.valueOf(threadIndex))));
            assertThat(thread.isDaemon(), is(equalTo(IS_DAEMON)));
            assertThat(thread.getPriority(), is(equalTo(DESIRED_PRIORITY)));
        }
    }
}
