package com.github.dant3.paperboy.utils;

import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class UtilsTest {
    @Test
    public void compareLongTest() {
        assertThat(Utils.compareLong(21L, 42L), is(lessThan(0)));
        assertThat(Utils.compareLong(42L, 21L), is(greaterThan(0)));
        assertThat(Utils.compareLong(42L, 42L), is(equalTo(0)));
    }
}
