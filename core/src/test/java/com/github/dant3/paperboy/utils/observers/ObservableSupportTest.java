package com.github.dant3.paperboy.utils.observers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ObservableSupportTest {
    @Mock private Observer<ObservableInteger> observer;
    private ObservableInteger integer;

    @Before public void before() {
        integer = new ObservableInteger();
    }

    @Test public void observableSupportCallsObserverThenNotifyTriggered() {
        integer.addObserver(observer);
        assertEquals(1, integer.observersCount());

        integer.notifyObservers();
        verify(observer, times(1)).handleUpdate(integer);
        verifyNoMoreInteractions(observer);
    }

    @Test public void observableSupportDoesNotCallsThenObserverRemoved() {
        observableSupportCallsObserverThenNotifyTriggered();
        assertEquals(1, integer.observersCount());

        integer.removeObserver(observer);
        assertEquals(0, integer.observersCount());
        integer.notifyObservers();
        verifyNoMoreInteractions(observer);
    }

    @Test public void observableSupportDoesNotCallsThanAllObserversRemoved() {
        observableSupportCallsObserverThenNotifyTriggered();
        assertEquals(1, integer.observersCount());

        integer.removeObservers();
        assertEquals(0, integer.observersCount());
        integer.notifyObservers();
        verifyNoMoreInteractions(observer);
    }

    private static class ObservableInteger extends ObservableSupport<ObservableInteger> {
        private int integer;

        public ObservableInteger() {
            this(0);
        }

        public ObservableInteger(int initialValue) {
            this.integer = initialValue;
        }

        public int get() {
            return integer;
        }

        public void set(int integer) {
            if (this.integer != integer) {
                this.integer = integer;
                notifyObservers();
            }
        }
    }
}
